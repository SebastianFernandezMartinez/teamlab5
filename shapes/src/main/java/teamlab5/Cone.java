//tannaz maghsoodi 2234772
package teamlab5;

public class Cone implements Shapes3d{
    private double height;
    private double radius;

    public Cone(double radius, double height) {
        if(radius <= 0 || height <= 0){
            throw new IllegalArgumentException("radius and height should not be negative");
        }
        this.height = height;
        this.radius = radius;
    }

    /**
     *
     * @return  radius of the cone
     */
    public double getRadius() {
        return this.radius;
    }

    /**
     *
     * @return  height of the cone
     */
    
    public double getHeight(){
        return this.height;
    }

    /**
     * @return the volume of the cone
     */
    @Override
    public double getVolume() {
        return Math.PI * Math.pow(this.radius, 2) * (this.height / 3.0);
    }
    
    /**
     * @return the surface area of the cone
     */
    @Override
    public double getSurfaceArea() {
        return Math.PI * this.radius * (this.radius + Math.sqrt(Math.pow(this.height, 2) + Math.pow(this.radius, 2)));
    }
}
