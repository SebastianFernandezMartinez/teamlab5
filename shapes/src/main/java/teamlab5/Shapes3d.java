package teamlab5;

public interface Shapes3d {
    double getVolume();
    double getSurfaceArea();
}
