//Sebastian Fernandez
package teamlab5;
/**
 * skeleton class that impletents the Shape3d interface.
 * this class only gives headers to give a insight on how these methods would be used.
 */
public class Cylinder implements Shapes3d{
    private double radius;
    private double height;

    public Cylinder(double radius, double height) throws IllegalArgumentException{
        if(radius <= 0 || height <= 0){
            throw new IllegalArgumentException("value cant be negatif");
        }
        this.radius = radius;
        this.height = height;
    }

    /**
     * 
     * @return  radius of the cylinder
     */
    public double getRadius(){
       return this.radius;
    }

    /**
     * 
     * @return  height of the cylinder
     */
    
    public double getHeight(){
        return this.height;
    }

    /**
     * @return the volume of the cylinder
     */
    @Override
    public double getVolume(){
        return Math.PI*(radius*radius)*height;
    }
     /**
     * @return the surface area of the cylinder
     */
    @Override
    public double getSurfaceArea(){
        return (2*Math.PI)*(radius*radius)+(2*Math.PI*radius*height);
    }
}
