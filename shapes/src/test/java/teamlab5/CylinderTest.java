//tannaz maghsoodi 2234772
package teamlab5;
import static org.junit.Assert.*;

import org.junit.Test;

public class CylinderTest {

    @Test
    public void testConstructor(){
        try{
            Cylinder cylinder = new Cylinder(5.0, 10.0);
            assertEquals(cylinder.getRadius(), 5.0,0);
            assertEquals(cylinder.getHeight(), 10.0,0);
        }
        catch(IllegalArgumentException e){
            assertTrue(true);
        }
    }

    @Test
    public void testHeightGetterMustReturn10() {

        Cylinder cylinder = new Cylinder(5.0, 10.0);
        assertEquals(10.0, cylinder.getHeight(),0);
    }

    @Test
    public void testRadiusGetterMustReturn5() {
        Cylinder cylinder = new Cylinder(5.0, 10.0);
        assertEquals(5.0, cylinder.getRadius(),0);
    }
    

    @Test
    public void testGetVolume() {
        Cylinder cylinder = new Cylinder(5.0, 10.0);
        double expectedVolume = Math.PI * Math.pow(5.0, 2) * 10.0;
        assertEquals(expectedVolume, cylinder.getVolume(),0);
    }

    @Test
    public void testGetSurfaceArea() {
        double radius = 5;
        double height = 10;
        Cylinder cylinder = new Cylinder(radius, height);
        double expectedSurfaceArea = ((2 * Math.PI)*(radius*radius)+(2 * Math.PI * radius * height));
        assertEquals(expectedSurfaceArea, cylinder.getSurfaceArea(),0);
    }
}
