//sebastian Fernandez
package teamlab5;

import static org.junit.Assert.*;

import org.junit.Test;

/** 
 * unit test for the Cone class
*/
public class ConeTest {


    /**
     * test if the constructor actually assigns the values to each field properly and if there is any mistake it throws the right exception
     */
    @Test
    public void testConstructor(){
        double radius = 5;
        double height = 10;
        try{
            Cone cone = new Cone(radius, height);
            assertEquals(cone.getRadius(), radius,0);
            assertEquals(cone.getHeight(), height,0);
        }
        catch(IllegalArgumentException e){
            assertTrue(true);
        }
    }

    /**
     * test if the get methods return the proper result
    */
    @Test
    public void testGetters(){
        double radius = 5;
        double height = 10;
        Cone cone = new Cone(radius, height);
        assertEquals(cone.getRadius(), radius,0);
        assertEquals(cone.getHeight(), height,0);
    }
    
    /**test if the volume it returns is the proper result */
    @Test
    public void testGetVolume(){
        double radius = 5;
        double height = 10;
        double volume  = Math.PI*(radius*radius)*(height/3);
        Cone cone = new Cone(radius, height);
        assertEquals(cone.getVolume(), volume,0);
    }

    /**
     * test if the surface area works as expected
     */
    @Test
    public void testGetSurfaceArea(){
        double radius = 5;
        double height = 10;
        double surfaceArea  = Math.PI*radius*(radius+Math.sqrt((height*height)+(radius*radius)));
        Cone cone = new Cone(radius, height);
        assertEquals(cone.getSurfaceArea(), surfaceArea,0);
    }
}

